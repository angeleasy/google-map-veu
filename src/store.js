import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios)

export default new Vuex.Store({
  state: {
    offices: [],
    active: null,
    isLoading: false,
    sendStatus: ''
  },
  mutations: {
    LOAD_OFFICIES(state, payload){
      state.offices = payload
    },
    SET_ACTIVE_OFFICE(state, payload){
      const activeOffice = state.offices.filter(item => {
        return item.id === payload
      })
      state.active = activeOffice[0]
    },
    SET_LOADING(state, payload) {
      state.isLoading = payload
    },
    SET_SEND_STATUS(state, payload) {
      state.sendStatus = payload
    }
  },
  actions: {
    fetchOfficies({commit}) {
      axios.get('http://localhost:3000/offices').then((response) => {
        const offices = response.data

        commit('LOAD_OFFICIES', offices)
        commit('SET_ACTIVE_OFFICE', offices[0].id)

      }).catch((error) => {
         // eslint-disable-next-line
        console.log(error)
      })
    },
    getOfficeById({commit}, payload) {
      commit('SET_ACTIVE_OFFICE', payload)
    },
    submitForm({commit}, payload) {
      commit('SET_LOADING', true)
      axios.post('http://httpbin.org/post', payload).then((response) => {
        commit('SET_LOADING', false)
        commit('SET_SEND_STATUS', 'success')
        // eslint-disable-next-line
        console.log(response.data)
      }).catch((error) => {
        commit('SET_LOADING', false)
        commit('SET_SEND_STATUS', 'error')
        // eslint-disable-next-line
        console.log(error)
      })
    }
  },
  getters: {
    getOffices: (state) => state.offices,
    getActiveOffice: (state) => state.active,
    getLoading: (state) => state.isLoading,
    getSendStatus: (state) => state.sendStatus
  }
})
